using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Pixelplacement.TweenSystem;
using System;

public class TweenTest : MonoBehaviour
{
    [SerializeField] AnimationCurve curve;
    [SerializeField] Tween.LoopType loopType;
    [SerializeField] float duration = 1f;
    [SerializeField] float delay = 0f;
    [SerializeField] TweenBase tweenPosition;

    void Start()
    {
        //Tween.Position(transform, transform.position, Vector3.right * 10, 1, 5);
        tweenPosition = Tween.Position(transform, Vector3.right * 10, duration, delay, curve, loopType, AnimationStart, AnimationEnd);
        Tween.Rotation(transform, Vector3.up * 90, duration, delay, Tween.EaseWobble, loopType);
    }

    private void AnimationStart()
    {
        //Debug.Log("AnimationStart");
    }

    private void AnimationEnd()
    {
        //Debug.Log("AnimationEnd");
    }

    private void Update()
    {
        // if the animation is playing - cancel, else - resume. (Maybe tweenPosition.Status)
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            tweenPosition.Cancel();
        }
        else
        {
            tweenPosition.Resume();
        }
    }
}
