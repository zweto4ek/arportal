using UnityEngine;
using System.Collections;
public class Portal : MonoBehaviour 
{
    [Header("Cameras")] 
    public Camera mainCamera; 
    public Camera portalCamera; 
    void Update() 
    { 
        Vector3 cameraOffset = mainCamera.transform.position - transform.position; 
        portalCamera.transform.position = transform.position + cameraOffset; 
        portalCamera.transform.rotation = Quaternion.LookRotation(mainCamera.transform.forward, Vector3.up);
    } 
    
    void OnTriggerEnter(Collider other) 
    { 
        if (other.CompareTag("MainCamera")) 
        { 
            mainCamera.cullingMask ^= 1 << LayerMask.NameToLayer("ARWorld");
            //portalCamera.cullingMask = mainCamera.cullingMask;
            //portalCamera.cullingMask ^= 1 << LayerMask.NameToLayer("ARWorld"); 
        }
    } 
}