using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _barrelPrefab;
    [SerializeField] private float _spawningDelay = 5f;
    [SerializeField] private Transform _mainPlatfrom;
    [SerializeField] private float _minOffset = 0.05f, _maxOffset = 0.1f;
    private float _platfromWidth, _platfromHeight, _platfromDepth;
    [SerializeField] private Material[] _materials;
    [SerializeField] private int _successCount, _failCount;
    [SerializeField, Range(0, 1)] private float _badBarrelChance;
    [SerializeField] private Material _badMaterial;
    [SerializeField] private ScoreManager _scoreManager;

    void Start()
    {
        _platfromWidth = _mainPlatfrom.localScale.x;
        _platfromHeight = _mainPlatfrom.position.y;
        _platfromDepth = _mainPlatfrom.position.z;

        StartSpawningBarrels();
    }

    public void StartSpawningBarrels()
    {
        InvokeRepeating("SpawnBarrels", 0, _spawningDelay);
    }

    private void SpawnBarrels()
    {
        float minY = _platfromHeight + _minOffset;
        float maxY = _platfromHeight + _maxOffset;
        // float x = Random.Range(0f, platformwidth) - platformwidth / 2;
        float posX = Random.Range(-_platfromWidth / 2, _platfromWidth / 2);
        float posY = Random.Range(minY, maxY);
        Vector3 position = new Vector3(posX, posY, _platfromDepth);

        int randMat = Random.Range(0, _materials.Length);
        float randBadBarrelChance = Random.Range(0f, 1f);
        GameObject _barrel = Instantiate(_barrelPrefab, position, Quaternion.Euler(Vector3.right * 90f));
        ColliderButton colliderButton = _barrel.GetComponent<ColliderButton>();
        colliderButton.OnClick.AddListener(_scoreManager.OnBarrelClick);
        if (randBadBarrelChance > _badBarrelChance)
        {
            _barrel.GetComponent<Renderer>().material = _materials[randMat];
        }
        else
        {
            _barrel.GetComponent<Renderer>().material = _badMaterial;
            _barrel.tag = "BadBarrel";
        }
    }

    public void AddSuccess()
    {
        _successCount++;
    }

    public void AddFail()
    {
        _failCount++;
        //ShrinkBarrel(failingBarrel.transform);
    }

    private void ShrinkBarrel( Transform failingBarrel)
    {

    }
}
