using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelCounter : MonoBehaviour
{
    private GameManager _gameManager;
    void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
        if (_gameManager == null)
        {
            Debug.LogError("Game Manager is NULL.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        BarrelManager _bm;
        if (!other.TryGetComponent<BarrelManager>(out _bm)) return;
            // count success if it's the right color in the right color box, otherwise
            // count failure if wrong color in box 
            if (other.GetComponent<Renderer>().material.color == GetComponent<Renderer>().material.color)
        {
            _gameManager.AddSuccess();

        }
        else
        {
            _gameManager.AddFail();
            _bm.ShrinkBarrel();
        }
    }
}