using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class RandomMover : MonoBehaviour
{
    private float _extendRangeMin = 0.05f, _extendRangeMax= 0.15f;
    private float _movementDurationMin = 2f, _movementDurationMax = 3.5f;
    private Vector3 centerPosition;
    private int _side = 1;

    void Start()
    {
        centerPosition = transform.position;
        Animate();
    }

    void Update()
    {
        
    }


    private void AnimationStart()
    {
        _side *= -1;
    }

    private void Animate()
    {
        float randomRangeX = Random.Range(_extendRangeMin, _extendRangeMax);
        float duration = Random.Range(_movementDurationMin, _movementDurationMax);
        //Vector3 position = Vector3.right * (centerPositionX + randomRangeX * _side);
        Vector3 position = new Vector3(centerPosition.x + randomRangeX * _side, centerPosition.y, centerPosition.z);
        Tween.Position(transform, position, duration, 0, Tween.EaseInOut, Tween.LoopType.None, AnimationStart, Animate);
    }
}
