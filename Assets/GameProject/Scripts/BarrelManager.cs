using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class BarrelManager : MonoBehaviour
{
    private ScoreManager _scoreManager;
    private float _dragMin = 15f, _draxMax = 25f;
    private float _implosionDuration = 0.1f, _implosionDelay = 0.5f;
    void Start()
    {
        try
        {
            _scoreManager = FindObjectOfType<ScoreManager>();
        }
        catch (System.Exception)
        {

            Debug.LogError("Score Manager is NULL.");
        }
        GetComponent<Rigidbody>().drag = Random.Range(_dragMin, _draxMax);
    }

    void Update()
    {

    }

    public void OnMouseClick(ColliderButton colliderButton)
    {
        Debug.Log("Click!");
        _scoreManager.OnBarrelClick(colliderButton);
    }

    public void ShrinkBarrel()
    {
        Tween.LocalScale(transform, transform.localScale * 0.2f, _implosionDuration, 0);
        Destroy(gameObject, _implosionDuration + _implosionDelay);
    }
}
