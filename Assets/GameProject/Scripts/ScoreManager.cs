using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private int score;
    [SerializeField] private TextMeshProUGUI _scoreText;
    private float explosionScale = 5, explosionDuration = 0.1f, explosionDelay = 0.05f;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnSelected(ColliderButton button)
    {
        Debug.Log(button.name);
    }

    public void OnBarrelClick(ColliderButton colliderButton)
    {
        Debug.Log(colliderButton.name);
        //Color barrelColor = colliderButton.GetComponent<MeshRenderer>().material.color;
        //if (barrelColor == Color.black)
        if (colliderButton.tag == "BadBarrel") score++;
        else score--;
        _scoreText.text = "Score: " + score;

        Debug.Log("Score = " + score);
        colliderButton.OnClick.RemoveListener(this.OnBarrelClick);
        Tween.LocalScale(colliderButton.transform, colliderButton.transform.localScale * explosionScale, explosionDuration, 0);
        Destroy(colliderButton.gameObject, explosionDuration + explosionDelay);
    }
}
