using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
}

public class Joystick : MonoBehaviour
{
    [SerializeField] private Slider _joystick;
    [SerializeField] private GameObject _platform;
    private enum SliderState
    {
        left,
        center,
        right
    }

    private SliderState _sliderState = SliderState.center;
    private bool _isDragging;
    private HingeJoint _platformHingeJoint;
    public FloatEvent RotationSpeed;

    void Start()
    {
        _platformHingeJoint = _platform.GetComponent<HingeJoint>();
        if (_platformHingeJoint == null)
        {
            Debug.LogError("The Platfrom Hinge Joint is NULL.");
        }
    }

    // we need to set UseSpring to true for center position of slider to return the platform to 0, after release the slider,
    // and we don't need to set it to false for right and left - WHY?????
    // to change event trigger from Begin Drag to Pointer Down, so the click to the slider range will also rotate the platform
    void Update()
    {
        if (!_isDragging)
        {
            if (_sliderState == SliderState.center)
            {
                _platformHingeJoint.useSpring = true;
            }
            else  if (_sliderState == SliderState.right)
            {
                //_platformHingeJoint.useSpring = false;
                _joystick.value -= Time.deltaTime;
                if (_joystick.value <= 0)
                {
                    _joystick.value = 0;
                    _sliderState = SliderState.center;
                }
            }
            else if (_sliderState == SliderState.left)
            {
                //_platformHingeJoint.useSpring = false;
                _joystick.value += Time.deltaTime;
                if (_joystick.value >= 0)
                {
                    _joystick.value = 0;
                    _sliderState = SliderState.center;
                }
            }
        }
        else
        {
            RotationSpeed.Invoke(_joystick.value);
        }
    }

    public void OnValueChanged(float value)
    {
        if (value == 0)
        {
            _sliderState = SliderState.center;
        }
        else if (value < 0)
        {
            _sliderState = SliderState.left;
        }
        else
        {
            _sliderState = SliderState.right;
        }
    }

    public float GetJoystickValue()
    {
        return _joystick.value;
    }

    public void StartDrag()
    {
        _sliderState = SliderState.center;
        //_platformHingeJoint.useSpring = false;
        _isDragging = true;
    }

    public void Drag()
    {
        Debug.Log("456");
    }

    public void PointerUp()
    {
        //_platformHingeJoint.useSpring = true;
        OnValueChanged(_joystick.value);
        _isDragging = false;
    }
}
