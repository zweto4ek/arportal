using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatfromRotator : MonoBehaviour
{
    private float _maxAngle = 35f;

    /*
    public void RotatePlatfrom(float rotationSpeed)
    {
        float _rotation = transform.rotation.eulerAngles.z;
        Debug.Log(_rotation);
        if ((_rotation > 0f && _rotation < _maxAngle) || (_rotation > (360f - _maxAngle)))
        {
            transform.Rotate(-Vector3.forward * rotationSpeed);
        }
        if ((_rotation > _maxAngle) && (_rotation < (_maxAngle + 10)))
        {
            transform.eulerAngles = Vector3.forward * _maxAngle;
        }
        if ((_rotation > (350 - _maxAngle)) && (_rotation < (360f - _maxAngle)))
        {
            transform.eulerAngles = Vector3.forward * (360 - _maxAngle);
        }
    }
    */

    public void RotatePlatfrom(float rotationSpeed)
    {
        float _rotation = transform.rotation.eulerAngles.z;
        if ((_rotation >= 0f && _rotation <= _maxAngle) || (_rotation > (360f - _maxAngle)))
        {
            transform.Rotate(-Vector3.forward * rotationSpeed);
        }
    }
    public void RotatePlatfrom(Vector2 rotationSpeed)
    {
        RotatePlatfrom(rotationSpeed.x);
    }
    public void RotateUpperPlatfromToLeft(Vector2 rotationSpeed)
    {
        RotatePlatfrom(rotationSpeed.y);
    }
    public void RotateUpperPlatfromToRight(Vector2 rotationSpeed)
    {
        RotatePlatfrom(-rotationSpeed.y);
    }
}
