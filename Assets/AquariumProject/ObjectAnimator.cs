using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Pixelplacement.TweenSystem;

public class ObjectAnimator : MonoBehaviour
{
    private enum AnimationType {RotationPingPong, RotationTranslation, ScalePingPong }
    [SerializeField] AnimationType myAnimation;
    [SerializeField] AnimationCurve myAnimationCurve;
    private float leftSide = -0.08f;
    private float rightSide = 0.085f;
    private float scaleOffset = 0.01f;
    private float timeAnim;

    void Start()
    {
        timeAnim = Random.Range(2.5f, 3.5f);
        switch (myAnimation)
        {
            case AnimationType.RotationPingPong:
                Vector3 targetScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                Tween.LocalScale(transform, targetScale, timeAnim, 0, Tween.EaseIn, Tween.LoopType.PingPong);
                //Tween.LocalRotation(transform, new Vector3(-180, 0, 0), timeAnim, 0, Tween.EaseIn, Tween.LoopType.PingPong);
                break;

            case AnimationType.RotationTranslation:
                RotationTranslation();
                break;

            case AnimationType.ScalePingPong:
                Vector3 targetScale2 = new Vector3(transform.localScale.x + scaleOffset, transform.localScale.y + scaleOffset, transform.localScale.z + scaleOffset);
                Tween.LocalScale(transform, targetScale2, timeAnim, timeAnim, Tween.EaseIn, Tween.LoopType.PingPong);
                break;

            default:
                break;
        }
    }

    private void RotationTranslation()
    {
        timeAnim = Random.Range(2.5f, 3.5f);
        Vector3 randPos = new Vector3(Random.Range(leftSide, rightSide), transform.localPosition.y, transform.localPosition.z);
        Tween.LocalPosition(transform, randPos, timeAnim, 0, myAnimationCurve, Tween.LoopType.None, null, RotationTranslation);
        /*
        Vector3 targetScale = transform.localScale;
        if (randPos.x < transform.localPosition.x)
        {
            targetScale.x = -transform.localScale.x;
        }
        Tween.LocalScale(transform, targetScale, 0.1f, 0, Tween.EaseIn, Tween.LoopType.None);
        */
        Tween.LocalScale(transform, new Vector3((randPos.x < transform.localPosition.x) ? -0.021f : 0.021f, 0.021f, 0.021f), 0.1f, 0, Tween.EaseIn, Tween.LoopType.None);
    }
}
